# -*- coding: utf-8 -*-
import mongoengine
import uuid
import os
import datetime

from operator import attrgetter
from enum import Enum

from flask import Flask, redirect, render_template, request, url_for, jsonify
from flask_mongoengine import MongoEngine
from mongoengine.queryset.visitor import Q
from flask_security import (
    UserMixin,
    RoleMixin,
    MongoEngineUserDatastore,
    login_required,
    Security,
)
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token,
    get_jwt_identity,
    set_access_cookies,
    jwt_required,
)

from flask_security.utils import verify_password
from flask_login import current_user


app = Flask(__name__)
app.config.from_pyfile("config.py")
app.logger.setLevel(0)

db = MongoEngine(app)

jwt = JWTManager(app)


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)


class User(db.Document, UserMixin):
    username = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    roles = db.ListField(db.ReferenceField(Role), default=[])


class Message(db.Document):
    sender = db.ReferenceField(User)
    recipient = db.ReferenceField(User)
    text = db.StringField()
    created_at = db.DateTimeField()


# Flask Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.before_first_request
def create_user():
    user_datastore.create_user(username="test1", password="password")
    user_datastore.create_user(username="test2", password="password2")


def dialog(uid1, uid2):
    return Message.objects.filter(
        Q(sender=uid1, recipient=uid2) | Q(sender=uid2, recipient=uid1)
    )


def last_message(uid1, uid2):
    last_message = dialog(uid1, uid2).order_by("-created_at").first()
    if last_message:
        return last_message.text
    return "No messages..."


def get_users_list():
    return [
        {
            "username": user.username,
            "last_message": last_message(user.id, current_user.id),
            "id": user.id,
        }
        for user in User.objects.all()
        if user.id != current_user.id
    ]


@app.route("/")
@login_required
def dialogs():
    return render_template("main.html", users=get_users_list(), dialog=None)


@app.route("/<string:user_id>")
@login_required
def user_dialogs(user_id):
    current_dialog = [
        {"text": message.text, "out": current_user.id == message.sender.id}
        for message in dialog(user_id, current_user.id)
    ]

    return render_template(
        "main.html",
        users=get_users_list(),
        dialog=current_dialog,
        user_id=user_id,
    )


@app.route("/send_message", methods=["POST"])
@login_required
def send_message():
    Message(
        recipient=request.form.get("recipient"),
        sender=current_user.id,
        text=request.form.get("message"),
        created_at=datetime.datetime.now(),
    ).save()

    return redirect(
        url_for("user_dialogs", user_id=request.form.get("recipient"))
    )


@app.route("/api/auth", methods=["POST"])
def auth():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = user_datastore.get_user(username)
    if user is None:
        return jsonify({"msg": "User does not exists"}), 401
    if not verify_password(password, user.password):
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    resp = jsonify({"token": access_token})
    return resp


if __name__ == "__main__":
    app.run(host="0.0.0.0")
