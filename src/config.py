SECRET_KEY = "123456790"

MONGODB_SETTINGS = {
    "authentication_source": "admin",
    "username": "tom",
    "password": "jerry",
    "host": "mongodb://db:27017/db",
}

SECURITY_USER_IDENTITY_ATTRIBUTES = "username"
SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
SECURITY_REGISTERABLE = True
SECURITY_PASSWORD_HASH = "sha512_crypt"
SECURITY_PASSWORD_SALT = "12345"

MEDIA_FOLDER = "/media/"


JWT_SECRET_KEY = "12dskfsldkfjasldnaslkdjalsdkjanldfmvn ,dmfnaslkmn"
