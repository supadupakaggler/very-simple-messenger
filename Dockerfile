FROM python:3.7

RUN apt update --fix-missing
RUN apt upgrade -y

COPY requirements.txt /tmp/

RUN pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /usr/src/app
COPY src .

ENV flask FLASK_APP:/usr/src/app/app.py
ENV PYTHONPATH "${PYTHONPATH}:/usr/src/app"
