import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";

const HomeScreen = ({ dialogs }) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={dialogs}
        renderItem={({ item }) => <Text style={styles.item}>{item}</Text>}
      />
    </View>
  );
};
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
