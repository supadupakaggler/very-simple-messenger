import { connect } from "react-redux";
import Container from "./container";

const getUserToken = (state) => ({
  token: state.token,
});

export default connect(getUserToken)(Container);
