import React, { Component, useEffect, useState } from "react";
import HomeScreen from "./presenter";

const Container = () => {
  const [dialogs, setDialogs] = useState([]);

  const fetchMessages = () => {
    console.debug("Fetching messages...");
    setDialogs([1, 2, 3]);
  };

  useEffect(() => {
    fetchMessages();
    setTimeout(fetchMessages, 5000);
  });

  return <HomeScreen dialogs={dialogs} />;
};

export default Container;
