import React from "react";
import { Button, Text } from "react-native";

import { TextInput } from "react-native";

const AuthScreen = (props) => (
  <>
    <Text style={{ textAlign: "center", marginTop: 10, fontSize: 20 }}>
      Very simple messager
    </Text>
    <TextInput
      placeholder="Username"
      value={props.username}
      onChangeText={props.changeUsername}
      style={{ height: 60, margin: 10 }}
    />
    <TextInput
      placeholder="Password"
      secureTextEntry={true}
      autoCapitalize={"none"}
      value={props.password}
      onChangeText={props.changePassword}
      style={{ height: 60, margin: 10 }}
    />
    <Button title="Login" onPress={props.submit} />
  </>
);

export default AuthScreen;
