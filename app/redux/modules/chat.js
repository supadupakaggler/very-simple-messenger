// Imports
import { API_URL } from "../../constants";
import { actionCreators as userActions } from "./user";

// API Actions
function fetchMesages(params) {
  return (dispatch, getState) => {
    const {
      user: { token },
    } = getState();

    return fetch(`${API_URL}/chats`, {
      method: "POST",
      headers: { Authorization: `Bearer ${token}` },
    }).then((response) => response.json());
  };
}

// Initial State
const initialState = {};

// Reducer
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

// Reducer Functions

// exports
const actionCreators = {
  fetchMesages,
};

export { actionCreators };

// export reducer by default

export default reducer;
